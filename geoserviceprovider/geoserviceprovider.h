#ifndef GEOSERVICEPROVIDER_H
#define GEOSERVICEPROVIDER_H

#include <QObject>
#include <QtLocation/QGeoServiceProviderFactory>

class GeoServiceProvider : public QObject, public QGeoServiceProviderFactory {
    Q_OBJECT
    Q_INTERFACES(QGeoServiceProviderFactory)
    Q_PLUGIN_METADATA(IID "org.qt-project.qt.geoservice.serviceproviderfactory/5.0" FILE "qtlocationplugin.json")

public:
    QGeoMappingManagerEngine* createMappingManagerEngine(const QVariantMap &parameters, QGeoServiceProvider::Error* error, QString* errorString) const Q_DECL_OVERRIDE;
    QPlaceManagerEngine* createPlaceManagerEngine(const QVariantMap &parameters, QGeoServiceProvider::Error *error, QString *errorString) const Q_DECL_OVERRIDE;
    QGeoRoutingManagerEngine* createRoutingManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString *errorString) const Q_DECL_OVERRIDE;
};

#endif // GEOSERVICEPROVIDER_H
