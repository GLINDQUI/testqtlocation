TARGET = geoserviceprovider
QT += location-private positioning-private network

PLUGIN_TYPE = geoservices
PLUGIN_CLASS_NAME = GeoServiceProvider
load(qt_plugin)

HEADERS += \
    geoserviceprovider.h \
    tilefetcher.h \
    tilemap.h \
    tilemapmanagerengine.h \
    searchreply.h \
    placemanagerengine.h \
    routingmanagerengine.h \
    routereply.h \
    mapreply.h

SOURCES += \
    geoserviceprovider.cpp \
    tilefetcher.cpp \
    tilemap.cpp \
    tilemapmanagerengine.cpp \
    searchreply.cpp \
    placemanagerengine.cpp \
    routingmanagerengine.cpp \
    routereply.cpp \
    mapreply.cpp

OTHER_FILES += \
    qtlocationplugin.json
