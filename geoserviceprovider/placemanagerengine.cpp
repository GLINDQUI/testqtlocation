#include "placemanagerengine.h"
#include "searchreply.h"

#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <QDebug>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtPositioning/QGeoCircle>
#include <QtPositioning/QGeoRectangle>

PlaceManagerEngine::PlaceManagerEngine(const QVariantMap &parameters, QGeoServiceProvider::Error *error, QString *errorString):
    QPlaceManagerEngine(parameters),
    m_networkManager(new QNetworkAccessManager(this)) {
    if (parameters.contains(QStringLiteral("google.useragent"))) {
        m_userAgent = parameters.value(QStringLiteral("google.useragent")).toString().toLatin1();
    } else {
        m_userAgent = "Qt Location based application";
    }

    m_apiKey = parameters.value(QStringLiteral("google.places.apikey")).toString();
    m_urlPrefix = parameters.value(QStringLiteral("google.places.baseuri")).toString();
    m_urlPath = parameters.value(QStringLiteral("google.places.basepath")).toString();

    *error = QGeoServiceProvider::NoError;
    errorString->clear();
}

QPlaceSearchReply* PlaceManagerEngine::search(const QPlaceSearchRequest& request) {
    bool unsupported = false;

    // Only public visibility supported
    unsupported |= request.visibilityScope() != QLocation::UnspecifiedVisibility && request.visibilityScope() != QLocation::PublicVisibility;
    unsupported |= request.searchTerm().isEmpty() && request.categories().isEmpty();
    unsupported |= m_apiKey.length() == 0 || m_urlPrefix.length() == 0 || m_urlPath.length() == 0;

    if (unsupported) {
        return QPlaceManagerEngine::search(request);
    }

    qreal radius = 0;
    QGeoShape searchArea = request.searchArea();
    switch (searchArea.type()) {
        case QGeoShape::CircleType: {
            QGeoCircle c(searchArea);
            radius = c.radius();
            break;
        }
        case QGeoShape::RectangleType: {
            QGeoRectangle boundingBox = searchArea;
            radius = qMax(boundingBox.width(), boundingBox.height()) / 2;
            break;
        }
        default: {
            radius = -1;
            break;
        }
    }

    QUrlQuery query;
    query.addQueryItem(QStringLiteral("query"), request.searchTerm());
    query.addQueryItem(QStringLiteral("key"), m_apiKey);
    query.addQueryItem(QStringLiteral("location"), QString("%0,%1").arg(request.searchArea().center().latitude()).arg(request.searchArea().center().longitude()));
    if (radius > -1) {
        query.addQueryItem(QStringLiteral("radius"), QString("%0").arg(radius));
    }

    QUrl requestUrl(QString("https://%0/%1/json").arg(m_urlPrefix).arg(m_urlPath));
    requestUrl.setQuery(query);

    SearchReply *reply = new SearchReply(request, m_networkManager->get(QNetworkRequest(requestUrl)), this);
    connect(reply, SIGNAL(finished()), this, SLOT(replyFinished()));
    connect(reply, SIGNAL(error(QPlaceReply::Error,QString)), this, SLOT(replyError(QPlaceReply::Error,QString)));

    return reply;
}

QList<QLocale> PlaceManagerEngine::locales() const {
    return m_locales;
}

void PlaceManagerEngine::setLocales(const QList<QLocale> &locales) {
    m_locales = locales;
}

void PlaceManagerEngine::replyFinished() {
    QPlaceReply *reply = qobject_cast<QPlaceReply *>(sender());
    if (reply) {
        emit finished(reply);
    }
}

void PlaceManagerEngine::replyError(QPlaceReply::Error errorCode, const QString& errorString) {
    QPlaceReply *reply = qobject_cast<QPlaceReply *>(sender());
    if (reply) {
        emit error(reply, errorCode, errorString);
    }
}
