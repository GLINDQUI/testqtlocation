#include "geoserviceprovider.h"
#include "tilemapmanagerengine.h"
#include "placemanagerengine.h"
#include "routingmanagerengine.h"

#include <QDebug>

QGeoMappingManagerEngine* GeoServiceProvider::createMappingManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString* errorString) const
{
    return new TileMapManagerEngine(parameters, error, errorString);
}

QPlaceManagerEngine*GeoServiceProvider::createPlaceManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString* errorString) const
{
    return new PlaceManagerEngine(parameters, error, errorString);
}

QGeoRoutingManagerEngine*GeoServiceProvider::createRoutingManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString* errorString) const
{
    return new RoutingManagerEngine(parameters, error, errorString);
}
