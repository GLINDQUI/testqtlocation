#ifndef PLACEMANAGERENGINE_H
#define PLACEMANAGERENGINE_H

#include <QtLocation/QPlaceManagerEngine>
#include <QtLocation/QGeoServiceProvider>

class QNetworkAccessManager;
class QNetworkReply;
class QPlaceCategoriesReplyTomtom;

class PlaceManagerEngine : public QPlaceManagerEngine {
    Q_OBJECT

public:
    PlaceManagerEngine(const QVariantMap &parameters, QGeoServiceProvider::Error *error, QString *errorString);

    QPlaceSearchReply* search(const QPlaceSearchRequest &request) Q_DECL_OVERRIDE;

    QList<QLocale> locales() const Q_DECL_OVERRIDE;
    void setLocales(const QList<QLocale> &locales) Q_DECL_OVERRIDE;

private slots:
    void replyFinished();
    void replyError(QPlaceReply::Error errorCode, const QString &errorString);

private:
    QNetworkAccessManager* m_networkManager;
    QByteArray m_userAgent;
    QString m_apiKey;
    QString m_urlPrefix;
    QString m_urlPath;
    QList<QLocale> m_locales;
};

#endif // PLACEMANAGERENGINE_H
