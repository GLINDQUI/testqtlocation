#ifndef TILEMAPMANAGERENGINE_H
#define TILEMAPMANAGERENGINE_H

#include <QGeoServiceProvider>
#include "QtLocation/private/qgeotiledmappingmanagerengine_p.h"

class TileMapManagerEngine: public QGeoTiledMappingManagerEngine {
    Q_OBJECT

public:
    TileMapManagerEngine(const QVariantMap &parameters, QGeoServiceProvider::Error* error, QString* errorString);

    virtual QGeoMap* createMap() Q_DECL_OVERRIDE;
};

#endif // TILEMAPMANAGERENGINE_H
