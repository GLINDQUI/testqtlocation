#include "searchreply.h"
#include "placemanagerengine.h"

#include <QDebug>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>
#include <QtLocation/QPlaceResult>
#include <QtLocation/QPlaceSearchRequest>
#include <QtNetwork/QNetworkReply>

SearchReply::SearchReply(const QPlaceSearchRequest& request, QNetworkReply* reply, PlaceManagerEngine* parent) :
    QPlaceSearchReply(parent),
    m_reply(reply) {
    setRequest(request);

    if (m_reply) {
        m_reply->setParent(this);
        connect(m_reply, SIGNAL(finished()), this, SLOT(replyFinished()));
    }
}

void SearchReply::abort() {
    if (m_reply) {
        m_reply->abort();
    }
}

QPlaceResult SearchReply::parsePlaceResult(const QJsonObject& item) const {
    QPlaceResult result;

    QPlace place;
    place.setPlaceId(item.value(QStringLiteral("id")).toString());
    place.setName(item.value(QStringLiteral("name")).toString());

    QVariantMap iconParameters;
    iconParameters.insert(QPlaceIcon::SingleUrl, QUrl(item.value(QStringLiteral("icon")).toString()));
    QPlaceIcon icon;
    icon.setParameters(iconParameters);
    place.setIcon(icon);

    QJsonObject geometry = item.value(QStringLiteral("geometry")).toObject();
    QJsonObject location = geometry.value(QStringLiteral("location")).toObject();
    QGeoCoordinate coordinate = QGeoCoordinate(location.value(QStringLiteral("lat")).toDouble(), location.value(QStringLiteral("lng")).toDouble());

    QGeoLocation geoLocation;
    QGeoAddress address;
    address.setText(item.value(QStringLiteral("formatted_address")).toString());
    geoLocation.setCoordinate(coordinate);
    geoLocation.setAddress(address);
    place.setLocation(geoLocation);

    result.setTitle(place.name());
    result.setPlace(place);
    result.setIcon(icon);

    return result;
}

void SearchReply::setError(QPlaceReply::Error errorCode, const QString& errorString) {
    QPlaceReply::setError(errorCode, errorString);
    emit error(errorCode, errorString);
    setFinished(true);
    emit finished();
}

void SearchReply::replyFinished() {
    QNetworkReply* reply = m_reply;
    m_reply->deleteLater();
    m_reply = 0;

    if (reply->error() != QNetworkReply::NoError) {
        setError(CommunicationError, tr("Communication error"));
        return;
    }

    QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
    if (document.isEmpty()) {
        setError(ParseError, tr("Response parse error"));
        return;
    }

    QGeoCoordinate searchCenter = request().searchArea().center();
    QList<QPlaceSearchResult> results;
    QStringList placeIds;
    QJsonArray resultsArray = document.object().value("results").toArray();
    for (int i = 0; i < resultsArray.count(); ++i) {
        QJsonObject item = resultsArray.at(i).toObject();
        QPlaceResult pr = parsePlaceResult(item);
        pr.setDistance(searchCenter.distanceTo(pr.place().location().coordinate()));
        placeIds.append(pr.place().placeId());
        results.append(pr);
    }

    setResults(results);

    setFinished(true);
    emit finished();
}
