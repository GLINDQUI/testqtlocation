#include "tilefetcher.h"
#include "mapreply.h"

#include <QDebug>

TileFetcher::TileFetcher(const QVariantMap& parameters, TileMapManagerEngine* engine, const QSize& tileSize): QGeoTileFetcher(engine),
    m_networkManager(new QNetworkAccessManager(this)),
    m_engine(engine),
    m_tileSize(tileSize) {
    m_apiKey = parameters["google.maps.apikey"].toString();
    m_baseUri = parameters["google.maps.baseuri"].toString();
    m_basePath = parameters["google.maps.basepath"].toString();

    m_networkRequest.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
    m_networkRequest.setRawHeader("Accept", "*/*");
}

QGeoTiledMapReply* TileFetcher::getTileImage(const QGeoTileSpec& spec) {
    if (m_apiKey.isEmpty()) {
        QGeoTiledMapReply* reply = new QGeoTiledMapReply(QGeoTiledMapReply::UnknownError, "Parameter google.maps.apikey unset", this);
        emit tileError(spec, reply->errorString());
        return reply;
    }

    if (m_baseUri.isEmpty()) {
        QGeoTiledMapReply* reply = new QGeoTiledMapReply(QGeoTiledMapReply::UnknownError, "Parameter google.maps.baseuri unset", this);
        emit tileError(spec, reply->errorString());
        return reply;
    }

    /*
     * blatantly stolen from sniffing google javascript tilemap example query
     * https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i10!2i271!3i383!4i256!2m3!1e0!2sm!3i355027289!3m9!2ssv!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&token=133
     */

    QString args = QString("pb=!1m5!1m4!1i%1!2i%2!3i%3!4i256!2m3!1e0!2sm!3i355027289!3m9!2ssv!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps").arg(spec.zoom()).arg(spec.x()).arg(spec.y());
    QUrl url = QUrl(QString("https://%1/%2/%3").arg(m_baseUri).arg(m_basePath).arg(args));

    m_networkRequest.setUrl(url);
    QNetworkReply* networkReply = m_networkManager->get(m_networkRequest);
    QGeoTiledMapReply* reply = new  MapReply(networkReply, spec);

    return reply;
}
