#ifndef TILEFETCHER_H
#define TILEFETCHER_H

#include "geoserviceprovider.h"
#include "tilemapmanagerengine.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include "QtLocation/private/qgeotilefetcher_p.h"
#include "QtLocation/private/qgeotilespec_p.h"

class TileFetcher : public QGeoTileFetcher {
    Q_OBJECT
    Q_DISABLE_COPY(TileFetcher)

public:
    TileFetcher(const QVariantMap &parameters, TileMapManagerEngine* engine, const QSize &tileSize);

    QGeoTiledMapReply* getTileImage(const QGeoTileSpec &spec) Q_DECL_OVERRIDE;

private:
    QNetworkAccessManager* m_networkManager;
    QNetworkRequest m_networkRequest;
    QNetworkReply* m_networkReply;

    QPointer<TileMapManagerEngine> m_engine;
    QSize m_tileSize;
    QString m_apiKey;
    QString m_baseUri;
    QString m_basePath;
};

#endif // TILEFETCHER_H
