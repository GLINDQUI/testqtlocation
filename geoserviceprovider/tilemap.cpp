#include "tilemap.h"
#include "tilemapmanagerengine.h"

#include <QDebug>

TileMap::TileMap(TileMapManagerEngine* engine, QObject* parent):
    QGeoTiledMap(engine, parent),
    m_engine(engine) {
}
