#include "tilemapmanagerengine.h"

#include "tilefetcher.h"
#include "tilemap.h"

#include <QDebug>

#include "QtLocation/private/qgeocameracapabilities_p.h"
#include "QtLocation/private/qgeofiletilecache_p.h"

TileMapManagerEngine::TileMapManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString* errorString)
{
    Q_UNUSED(error)
    Q_UNUSED(errorString)

    QGeoCameraCapabilities capabilities;
    capabilities.setMinimumZoomLevel(0.0);
    capabilities.setMaximumZoomLevel(18.0);
    setCameraCapabilities(capabilities);

    setTileSize(QSize(256, 256));
    setTileFetcher(new TileFetcher(parameters, this, tileSize()));

    setTileCache(new QGeoFileTileCache(QAbstractGeoTileCache::baseCacheDirectory() + "tilemap"));
}

QGeoMap* TileMapManagerEngine::createMap()
{
    return new TileMap(this);
}
