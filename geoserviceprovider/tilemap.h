#ifndef TILEMAP_H
#define TILEMAP_H

#include <QtCore/QPointer>
#include "QtLocation/private/qgeotiledmap_p.h"

class TileMapManagerEngine;

class TileMap : public QGeoTiledMap {
    Q_OBJECT
    Q_DISABLE_COPY(TileMap)

public:
    TileMap(TileMapManagerEngine* engine, QObject* parent = 0);

private:
    QPointer<TileMapManagerEngine> m_engine;
};

#endif // TILEMAP_H
