#include "routereply.h"

#include <QDebug>
#include <QGeoCoordinate>
#include <QGeoManeuver>
#include <QGeoRectangle>
#include <QGeoRouteSegment>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>

RouteReply::RouteReply(QNetworkReply* reply, const QGeoRouteRequest& request, QObject* parent):
    QGeoRouteReply(request, parent),
    m_reply(reply) {
    connect(m_reply, SIGNAL(finished()), this, SLOT(networkReplyFinished()));
    connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(networkReplyError(QNetworkReply::NetworkError)));
}

static QGeoCoordinate convertCoordinate(QJsonObject location) {
    QGeoCoordinate result;

    result.setLatitude(location.value(QStringLiteral("lat")).toDouble());
    result.setLongitude(location.value(QStringLiteral("lng")).toDouble());

    return result;
}

static QGeoManeuver::InstructionDirection convertDirection(QString maneuver) {
    QGeoManeuver::InstructionDirection result = QGeoManeuver::NoDirection;

    if (maneuver == "straight" || maneuver == "merge") {
        result = QGeoManeuver::DirectionForward;
    } else if (maneuver == "fork-right" || maneuver == "ramp-right" || maneuver == "keep-right") {
        result = QGeoManeuver::DirectionBearRight;
    } else if (maneuver == "fork-left" || maneuver == "ramp-left"|| maneuver == "keep-left") {
        result = QGeoManeuver::DirectionBearLeft;
    } else if (maneuver == "turn-slight-right") {
        result = QGeoManeuver::DirectionLightRight;
    } else if (maneuver == "turn-slight-left") {
        result = QGeoManeuver::DirectionLightLeft;
    } else if (maneuver == "turn-right" || maneuver == "roundabout-right") {
        result = QGeoManeuver::DirectionRight;
    } else if (maneuver == "turn-left" || maneuver == "roundabout-left") {
        result = QGeoManeuver::DirectionLeft;
    } else if (maneuver == "turn-sharp-right") {
        result = QGeoManeuver::DirectionHardRight;
    } else if (maneuver == "turn-sharp-left") {
        result = QGeoManeuver::DirectionHardLeft;
    } else if (maneuver == "uturn-right") {
        result = QGeoManeuver::DirectionUTurnRight;
    } else if (maneuver == "uturn-left") {
        result = QGeoManeuver::DirectionUTurnLeft;
    } else {
        result = QGeoManeuver::NoDirection;
    }

    return result;
}

static QGeoManeuver convertManeuver(QJsonObject step) {
    QGeoManeuver result;

    result.setDirection(convertDirection(step.value(QStringLiteral("maneuver")).toString()));
    result.setInstructionText(step.value(QStringLiteral("html_instructions")).toString());
    result.setPosition(convertCoordinate(step.value(QStringLiteral("start_location")).toObject()));

    return result;
}

static QList<QGeoCoordinate> decodePolyline(QJsonObject polyline) {
    QList<QGeoCoordinate> result;

    QString points = polyline.value(QStringLiteral("points")).toString();

    int len = points.length();
    int index = 0;

    double lat = 0;
    double lng = 0;

    int shift = 0;
    int value = 0;
    char b;

    while (index < len) {
        shift = 0;
        value = 0;
        do {
            b = points.at(index++).toLatin1() - 63;
            value |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        double dlat = ((value & 1) ? ~(value >> 1) : (value >> 1));
        lat += dlat;

        shift = 0;
        value = 0;
        do {
            b = points.at(index++).toLatin1() - 63;
            value |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        double dlng = ((value & 1) ? ~(value >> 1) : (value >> 1));
        lng += dlng;

        result.append(QGeoCoordinate(lat * (double)1e-5, lng * (double)1e-5));
    }

    return result;
}

static QGeoRouteSegment convertSegment(QJsonObject step) {
    QGeoRouteSegment result;

    result.setDistance(step.value(QStringLiteral("distance")).toObject().value(QStringLiteral("value")).toDouble());
    result.setTravelTime(step.value(QStringLiteral("duration")).toObject().value(QStringLiteral("value")).toInt());
    result.setPath(decodePolyline(step.value(QStringLiteral("polyline")).toObject()));
    result.setManeuver(convertManeuver(step));

    return result;
}

void RouteReply::abort() {
    if (!m_reply) {
        return;
    }

    m_reply->abort();

    m_reply->deleteLater();
    m_reply = 0;
}

void RouteReply::networkReplyFinished() {
    if (!m_reply) {
        return;
    }

    if (m_reply->error() != QNetworkReply::NoError) {
        setError(QGeoRouteReply::CommunicationError, m_reply->errorString());
        m_reply->deleteLater();
        m_reply = 0;
        return;
    }

    QJsonDocument document = QJsonDocument::fromJson(m_reply->readAll());
    QList<QGeoRoute> result;
    if (document.isObject()) {
        QJsonObject errorObject = document.object().value(QStringLiteral("error")).toObject();
        if (!errorObject.isEmpty()) {
            setError(QGeoRouteReply::UnknownError, errorObject.value(QStringLiteral("description")).toString());
            m_reply->deleteLater();
            m_reply = 0;
            return;
        }

        QString status = document.object().value(QStringLiteral("status")).toString();
        if (status.toLower() != "ok") {
            setError(QGeoRouteReply::UnknownError, QString("No route received from Google API, status = %0").arg(status));
            m_reply->deleteLater();
            m_reply = 0;
            return;
        }

        QJsonArray routes = document.object().value(QStringLiteral("routes")).toArray();
        for (int i = 0; i < routes.count(); i++) {
            result.append(QGeoRoute());

            QJsonObject route = routes.at(i).toObject();

            QJsonObject bounds = route.value(QStringLiteral("bounds")).toObject();
            QList<QGeoCoordinate> boundingCoordinates;
            boundingCoordinates.append(convertCoordinate(bounds.value(QStringLiteral("northeast")).toObject()));
            boundingCoordinates.append(convertCoordinate(bounds.value(QStringLiteral("southwest")).toObject()));
            result.last().setBounds(QGeoRectangle(boundingCoordinates));

            qreal distance = 0.0;
            quint64 travelTime = 0;
            QList<QGeoCoordinate> path;

            QJsonArray legs = route.value(QStringLiteral("legs")).toArray();
            for (int i = 0; i < legs.count(); i++) {
                QJsonObject leg = legs.at(i).toObject();
                distance += leg.value(QStringLiteral("distance")).toObject().value(QStringLiteral("value")).toDouble();
                travelTime += leg.value(QStringLiteral("duration")).toObject().value(QStringLiteral("value")).toInt();

                QJsonArray steps = leg.value(QStringLiteral("steps")).toArray();
                if (steps.count() > 0) {
                    QJsonObject step = steps.first().toObject();
                    QGeoRouteSegment currentSegment = convertSegment(step);
                    path.append(currentSegment.path());
                    result.last().setFirstRouteSegment(currentSegment);

                    for (int j = 1; j < steps.count(); j++) {
                        QJsonObject step = steps.at(j).toObject();

                        QGeoRouteSegment segment = convertSegment(step);
                        path.append(segment.path());
                        currentSegment.setNextRouteSegment(segment);
                        currentSegment = currentSegment.nextRouteSegment();
                    }
                }
            }

            result.last().setDistance(distance);
            result.last().setTravelTime(travelTime);
            result.last().setPath(path);
        }

        if (result.count() > 0) {
            setRoutes(result);
            setFinished(true);
        } else {
            setError(QGeoRouteReply::UnknownError, QString("No route received from Google API, status = %0").arg(status));
        }
    } else {
        setError(QGeoRouteReply::ParseError, QStringLiteral("Error parsing Google API response"));
    }

    m_reply->deleteLater();
    m_reply = 0;
}

void RouteReply::networkReplyError(QNetworkReply::NetworkError error) {
    Q_UNUSED(error)

    if (!m_reply) {
        return;
    }

    setError(QGeoRouteReply::CommunicationError, m_reply->errorString());

    m_reply->deleteLater();
    m_reply = 0;
}
