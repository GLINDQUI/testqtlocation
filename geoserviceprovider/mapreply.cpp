#include "mapreply.h"

#include <QNetworkAccessManager>
#include <QNetworkCacheMetaData>

MapReply::MapReply(QNetworkReply* reply, const QGeoTileSpec &spec, QObject* parent):
    QGeoTiledMapReply(spec, parent),
    m_reply(reply) {
    connect(m_reply, SIGNAL(finished()), this, SLOT(networkFinished()));
    connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(networkError(QNetworkReply::NetworkError)));
}

QNetworkReply* MapReply::networkReply() const {
    return m_reply;
}

void MapReply::abort() {
    if (m_reply) {
        m_reply->abort();
    }
}

void MapReply::networkFinished() {
    if (m_reply && m_reply->error() == QNetworkReply::NoError) {
        setMapImageData(m_reply->readAll());
        setMapImageFormat("png");
        setFinished(true);

        m_reply->deleteLater();
        m_reply = 0;
    }
}

void MapReply::networkError(QNetworkReply::NetworkError error) {
    if (m_reply) {
        if (error != QNetworkReply::OperationCanceledError) {
            setError(QGeoTiledMapReply::CommunicationError, m_reply->errorString());
        }
        setFinished(true);
        m_reply->deleteLater();
        m_reply = 0;
    }
}
