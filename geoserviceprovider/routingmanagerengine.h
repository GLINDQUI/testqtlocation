#ifndef ROUTINGMANAGERENGINE_H
#define ROUTINGMANAGERENGINE_H

#include <QtLocation/QGeoServiceProvider>
#include <QtLocation/QGeoRoutingManagerEngine>

class QNetworkAccessManager;

class RoutingManagerEngine : public QGeoRoutingManagerEngine
{
    Q_OBJECT

public:
    RoutingManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString* errorString);
    QGeoRouteReply* calculateRoute(const QGeoRouteRequest& request) Q_DECL_OVERRIDE;

private slots:
    void replyFinished();
    void replyError(QGeoRouteReply::Error errorCode, const QString &errorString);

private:
    QNetworkAccessManager* m_networkManager;
    QByteArray m_userAgent;
    QString m_apiKey;
    QString m_urlPrefix;
    QString m_urlPath;
};

#endif // ROUTINGMANAGERENGINE_H
