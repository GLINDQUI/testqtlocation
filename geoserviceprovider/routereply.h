#ifndef ROUTEREPLY_H
#define ROUTEREPLY_H

#include <QtNetwork/QNetworkReply>
#include <QtLocation/QGeoRouteReply>

class RouteReply : public QGeoRouteReply
{
    Q_OBJECT

public:
    RouteReply(QNetworkReply* reply, const QGeoRouteRequest& request, QObject* parent = 0);

    void abort() Q_DECL_OVERRIDE;

private slots:
    void networkReplyFinished();
    void networkReplyError(QNetworkReply::NetworkError error);

private:
    QNetworkReply* m_reply;
};

#endif // ROUTEREPLY_H
