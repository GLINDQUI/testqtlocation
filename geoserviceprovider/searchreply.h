#ifndef SEARCHREPLY_H
#define SEARCHREPLY_H

#include <QtLocation/QPlaceSearchReply>

class QNetworkReply;
class PlaceManagerEngine;
class QPlaceResult;

class SearchReply : public QPlaceSearchReply {
    Q_OBJECT

public:
    SearchReply(const QPlaceSearchRequest& request, QNetworkReply* reply, PlaceManagerEngine* parent);

    void abort() Q_DECL_OVERRIDE;

private:
    QNetworkReply* m_reply;

    QPlaceResult parsePlaceResult(const QJsonObject &item) const;

private slots:
    void setError(QPlaceReply::Error errorCode, const QString &errorString);
    void replyFinished();
};

#endif // SEARCHREPLY_H
