#ifndef MAPREPLY_H
#define MAPREPLY_H

#include <QPointer>
#include <QtNetwork/QNetworkReply>
#include <QtLocation/private/qgeotilespec_p.h>
#include <QtLocation/private/qgeotiledmapreply_p.h>

class MapReply : public QGeoTiledMapReply {
    Q_OBJECT

public:
    MapReply(QNetworkReply* reply, const QGeoTileSpec &spec, QObject* parent = 0);

    void abort() Q_DECL_OVERRIDE;

    QNetworkReply* networkReply() const;

private slots:
    void networkFinished();
    void networkError(QNetworkReply::NetworkError error);

private:
    QPointer<QNetworkReply> m_reply;
};

#endif // MAPREPLY_H
