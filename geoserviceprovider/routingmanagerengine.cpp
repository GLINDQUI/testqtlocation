#include "routingmanagerengine.h"
#include "routereply.h"

#include <QDebug>
#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>

RoutingManagerEngine::RoutingManagerEngine(const QVariantMap& parameters, QGeoServiceProvider::Error* error, QString* errorString):
    QGeoRoutingManagerEngine(parameters),
    m_networkManager(new QNetworkAccessManager(this)) {
    if (parameters.contains(QStringLiteral("google.useragent"))) {
        m_userAgent = parameters.value(QStringLiteral("google.useragent")).toString().toLatin1();
    } else {
        m_userAgent = "Qt Location based application";
    }

    m_apiKey = parameters.value(QStringLiteral("google.directions.apikey")).toString();
    m_urlPrefix = parameters.value(QStringLiteral("google.directions.baseuri")).toString();
    m_urlPath = parameters.value(QStringLiteral("google.directions.basepath")).toString();

    *error = QGeoServiceProvider::NoError;
    errorString->clear();
}

QGeoRouteReply* RoutingManagerEngine::calculateRoute(const QGeoRouteRequest& request) {
    QNetworkRequest networkRequest;
    networkRequest.setRawHeader("User-Agent", m_userAgent);

    if (m_apiKey.isEmpty()) {
        QGeoRouteReply *reply = new QGeoRouteReply(QGeoRouteReply::UnsupportedOptionError, "Set google.directions.apikey with Google API key", this);
        emit error(reply, reply->error(), reply->errorString());
        return reply;
    }

    if (request.waypoints().count() < 2) {
        QGeoRouteReply *reply = new QGeoRouteReply(QGeoRouteReply::UnsupportedOptionError, "No destination set", this);
        emit error(reply, reply->error(), reply->errorString());
        return reply;
    }

    QUrlQuery query;
    query.addQueryItem(QStringLiteral("origin"), QString("%0,%1").arg(request.waypoints().first().latitude()).arg(request.waypoints().first().longitude()));
    query.addQueryItem(QStringLiteral("destination"), QString("%0,%1").arg(request.waypoints().last().latitude()).arg(request.waypoints().last().longitude()));

    if (request.waypoints().count() > 2) {
        QString waypoints;
        for (int i = 1; i < request.waypoints().count(); i++) {
            waypoints += QString("%0%1,%2").arg(waypoints.length() > 0 ? "|" : "").arg(request.waypoints().at(i).latitude()).arg(request.waypoints().at(i).longitude());
        }
    }

    /*
    if (request.travelModes() & QGeoRouteRequest::CarTravel)
        query.addQueryItem(QStringLiteral("travelMode"), QStringLiteral("car"));
    else if (request.travelModes() & QGeoRouteRequest::TruckTravel)
        query.addQueryItem(QStringLiteral("travelMode"), QStringLiteral("truck"));
    else if (request.travelModes() & QGeoRouteRequest::PedestrianTravel)
        query.addQueryItem(QStringLiteral("travelMode"), QStringLiteral("pedestrian"));
    else if (request.travelModes() & QGeoRouteRequest::BicycleTravel)
        query.addQueryItem(QStringLiteral("travelMode"), QStringLiteral("bicycle"));
    else if (request.travelModes() & QGeoRouteRequest::PublicTransitTravel)
        query.addQueryItem(QStringLiteral("travelMode"), QStringLiteral("bus"));

    foreach (QGeoRouteRequest::FeatureType routeFeature, request.featureTypes()) {
        QGeoRouteRequest::FeatureWeight weigth = request.featureWeight(routeFeature);
        if (weigth == QGeoRouteRequest::AvoidFeatureWeight
                || weigth == QGeoRouteRequest::DisallowFeatureWeight) {
            if (routeFeature == QGeoRouteRequest::TollFeature)
                query.addQueryItem(QStringLiteral("avoid"), QStringLiteral("tollRoads"));
            if (routeFeature == QGeoRouteRequest::HighwayFeature)
                query.addQueryItem(QStringLiteral("avoid"), QStringLiteral("motorways"));
            if (routeFeature == QGeoRouteRequest::FerryFeature)
                query.addQueryItem(QStringLiteral("avoid"), QStringLiteral("ferries"));
            if (routeFeature == QGeoRouteRequest::MotorPoolLaneFeature)
                query.addQueryItem(QStringLiteral("avoid"), QStringLiteral("carpools"));
            if (routeFeature == QGeoRouteRequest::DirtRoadFeature)
                query.addQueryItem(QStringLiteral("avoid"), QStringLiteral("unpavedRoads"));
        }
    }

    if (request.routeOptimization() & QGeoRouteRequest::FastestRoute)
        query.addQueryItem(QStringLiteral("routeType"), QStringLiteral("fastest"));
    else if (request.routeOptimization() & QGeoRouteRequest::ShortestRoute)
        query.addQueryItem(QStringLiteral("routeType"), QStringLiteral("shortest"));
    else if (request.routeOptimization() & QGeoRouteRequest::MostEconomicRoute)
        query.addQueryItem(QStringLiteral("routeType"), QStringLiteral("eco"));
    else if (request.routeOptimization() & QGeoRouteRequest::MostScenicRoute)
        query.addQueryItem(QStringLiteral("routeType"), QStringLiteral("thrilling"));

    query.addQueryItem(QStringLiteral("routeRepresentation"),
                       QStringLiteral("polyline"));

    query.addQueryItem(QStringLiteral("instructionsType"),
                       QStringLiteral("text"));

    const QLocale loc(locale());

    if (QLocale::C != loc.language() && QLocale::AnyLanguage != loc.language()) {
        query.addQueryItem(QStringLiteral("language"), loc.name());
    }
    */

    query.addQueryItem(QStringLiteral("key"), m_apiKey);

    QUrl requestUrl(QString("https://%0/%1/json").arg(m_urlPrefix).arg(m_urlPath));
    requestUrl.setQuery(query);
    networkRequest.setUrl(requestUrl);

    QNetworkReply *reply = m_networkManager->get(networkRequest);

    RouteReply *routeReply = new RouteReply(reply, request, this);

    connect(routeReply, SIGNAL(finished()), this, SLOT(replyFinished()));
    connect(routeReply, SIGNAL(error(QGeoRouteReply::Error,QString)), this, SLOT(replyError(QGeoRouteReply::Error,QString)));

    return routeReply;
}

void RoutingManagerEngine::replyFinished() {
    QGeoRouteReply* reply = qobject_cast<QGeoRouteReply*>(sender());
    if (reply) {
        emit finished(reply);
    }
}

void RoutingManagerEngine::replyError(QGeoRouteReply::Error errorCode, const QString& errorString) {
    QGeoRouteReply* reply = qobject_cast<QGeoRouteReply*>(sender());
    if (reply) {
        emit error(reply, errorCode, errorString);
    }
}
