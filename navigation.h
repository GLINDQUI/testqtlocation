#ifndef NAVIGATION_H
#define NAVIGATION_H

#include <QObject>

#include <QGeoRouteReply>
#include <QGeoRouteSegment>
#include <QGeoServiceProvider>
#include <QPlaceReply>
#include <QPlaceSearchReply>

#include "JavascriptInterfaceNavigation.h"

class Navigation : public QObject
{
    Q_OBJECT
private:
    JavascriptInterfaceNavigation _naviInterface;
    QString _provider;
    QVariantMap _parameters;
    QGeoServiceProvider* _serviceProvider;
    QPlaceManager* _placeManager;
    QGeoRoutingManager* _routingManager;
    QPlaceSearchReply* _searchReply;

    QList<QGeoCoordinate> _path;
    QVariantMap _currentPosition;
    QVariantMap _previousPosition;

    QGeoRouteSegment _currentSegment;
    QGeoRouteSegment _nextSegment;

    void initServiceProvider();
    void initManagers();
    QVariantMap geoCoordinateToVariantMap(QGeoCoordinate coordinate);
    double calculateDistance(QGeoCoordinate position, QGeoCoordinate start, QGeoCoordinate end);
    double calculateRemainingDistance();
    void calculateRouteTo(QGeoCoordinate destination);
    void clearRoute();
    QGeoCoordinate previousPosition();
    void emitNextManeuver();
    void emitRemainingDistance();

public:
    explicit Navigation(QObject *parent = 0);
    ~Navigation();

    Q_INVOKABLE QString provider();
    Q_INVOKABLE QVariantMap pluginParameters();
    Q_INVOKABLE QGeoCoordinate currentPosition();
    Q_INVOKABLE double currentHeading();
    Q_INVOKABLE void calculateRouteTo(QVariant destination);
    Q_INVOKABLE void findRouteTo(QString destination);

    // debug
    Q_INVOKABLE void nextSegment();

signals:
    void positionUpdated(QGeoCoordinate geoCoordinate);
    void headingUpdated(double heading);
    void routeAvailable(QVariantList path, QGeoCoordinate start,QGeoCoordinate end);
    void routeEmpty();
    void destinationSet(QString name, QGeoCoordinate location);
    void nextManeuver(QVariantMap maneuver);
    void remainingDistance(double distance);

    // debug
    void currentPath(QVariantList path);
    void nextInstruction(QGeoCoordinate position);
    void markerLine(QGeoCoordinate start, QGeoCoordinate end);

private slots:
    void onPositionUpdated(QVariant position);
    void onRouteError(QGeoRouteReply* reply, QGeoRouteReply::Error error, QString errorString);
    void onRouteFinished(QGeoRouteReply* reply);
    void onSearchError(QPlaceReply* reply, QPlaceReply::Error error, QString errorString);
    void onSearchFinished(QPlaceReply* reply);
};

#endif // NAVIGATION_H
