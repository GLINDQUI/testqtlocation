#include <QQmlContext>
#include "SpaApplication.h"
#include "SpaApplicationViewer.h"
#include "navigation.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    SpaApplication app(argc, argv);
    SpaApplicationViewer *viewer = SpaApplicationViewer::instance();

    QSharedPointer<Navigation> navigation = QSharedPointer<Navigation>(new Navigation());
    viewer->rootContext()->setContextProperty("QtNavigation", navigation.data());

    viewer->setSource(QStringLiteral("qml/main.qml"));
    viewer->show();
    return app.exec();
}
