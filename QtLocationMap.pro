TEMPLATE = app

greaterThan(QT_MAJOR_VERSION, 4) {
    CONFIG += c++11
} else {
    QMAKE_CXXFLAGS += -std=c++11
}

QT += quick network location
CONFIG += exceptions

qmlsources.source   = qml
qmlsources.target   = .
DEPLOYMENTFOLDERS   += qmlsources

include (../SpaApplicationFramework/src/SpaApplicationFramework.pri)

DEFINES += \
    EXCLUDE_MEDIAPLATFORMONITOR \
    EXCLUDE_MEDIAPLAYER \
    EXCLUDE_PHONE \
    EXCLUDE_CONTACTS \
    EXCLUDE_AUDIOINPUT \
    EXCLUDE_XMLPARSER \
    EXCLUDE_SPEECH \
    EXCLUDE_RECENTSOURCES

HEADERS += \
    navigation.h

SOURCES += main.cpp \
    navigation.cpp

OTHER_FILES += \
    ManifestFile.ini \
    qml/main.qml \
    qml/views/* \
    qml/js/controllers/viewControllers/* \
    qml/js/controllers/appController/* \
    qml/qmlComponents/*

applicationDeployment()
