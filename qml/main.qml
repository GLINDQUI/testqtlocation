// version 0x10016
/* Copyright (c) 2015 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import QtPositioning 5.3
import QtLocation 5.3
import SpaComponents 1.0

import "views"
import "js/controllers/appController/AppController.js" as AppController
import "js/controllers/viewControllers/MaximizedViewController.js" as MaximizedViewController
import "js/controllers/viewControllers/LibraryStack1RootViewController.js" as LibraryStack1RootViewController
import "js/controllers/viewControllers/LibraryStack1ViewController.js" as LibraryStack1ViewController

SpaApplicationArea {
    id: appContainer


    /////////////////////////////////////////////////////
    //////////// App metadata strings ///////////////////
    /////////////////////////////////////////////////////

    //: Applications name
    //% "Template app"
    property string appName: qsTrId("00X_APP_NAME");

    //: A longer description of the app. Shown in OTA download store.
    //% "The template app is a terrific app that can be used as a template for new apps as well as old. Adhering to the conventions used in the template app makes the app easier to understand and maintain. Best practices is documented as part of the template app"
    property string appDescription: qsTrId("00X_APP_DESC_LONG");

    //: A shorter description of the app. Shown in OTA download store.
    //% "A template app"
    property string appShortDescription: qsTrId("00X_APP_DESC_SHORT");

    property string viewState: "MAXIMIZED_VIEW"
    property string market: "Europe"


    /////////////////////////////////////////////////////
    ////////////////// View Controllers /////////////////
    /////////////////////////////////////////////////////

    // Need to be declared here since the controllers cannot
    // be injected at app startup into views that are not created yet
    // (views will be pushed on the library stack when needed).
    property var appController: null
    property var maximizedViewController: null
    property var libraryStack1RootViewController: null
    property var libraryStack1ViewController: null


    /////////////////////////////////////////////////////
    ////////////////// Maximized Views //////////////////
    /////////////////////////////////////////////////////

    MaximizedView {
        id: maximizedView
    }

    /////////////////////////////////////////////////////
    ////////////////// Library Stack Roots //////////////
    /////////////////////////////////////////////////////

    LibraryStack1RootView {
        id: libraryStack1RootView
    }


    /////////////////////////////////////////////////////
    /////////////////// View Models /////////////////////
    /////////////////////////////////////////////////////

    QtObject {
        id: maximizedViewState

        property string state: "initializing"

        onStateChanged: {
            Log.debug("QtLocationMap", "main.qml: maximizedView.state changed to " + state);
        }
    }

    QtObject {
        id: mapState

        property string provider: QtNavigation.provider()
        property var parameters: QtNavigation.pluginParameters()
        property var currentPosition: QtNavigation.currentPosition()
        property double currentHeading: QtNavigation.currentHeading()
        property string currentDestinationName: ""
        property var currentDestinationLocation

        property var currentRoute: { return []; }
        property var currentRouteStartpoint: { return {}; }
        property var currentRouteEndpoint: { return {}; }
    }

    ListModel {
        id: mapTypesList
    }

    // debug
    QtObject {
        id: debugState

        property var nextPosition
        property var markerLine: { return []; }
        property var currentPath: { return []; }
                                            /*
                                            {"latitude":57.7293456,"longitude":11.8545699},
                                            {"latitude":57.7291954,"longitude":11.8546128},
                                            {"latitude":57.7291954,"longitude":11.8546128},
                                            {"latitude":57.7287447,"longitude":11.8545055},
                                            {"latitude":57.7285838,"longitude":11.8544197},
                                            {"latitude":57.7285838,"longitude":11.8544197},
                                            {"latitude":57.7270067,"longitude":11.8530357},
                                            {"latitude":57.7270067,"longitude":11.8530357},
                                            {"latitude":57.7267599,"longitude":11.8528211},
                                            {"latitude":57.726438,"longitude":11.8526924},
                                            {"latitude":57.726438,"longitude":11.8526924},
                                            {"latitude":57.7256334,"longitude":11.8529713},
                                            {"latitude":57.7256334,"longitude":11.8529713},
                                            {"latitude":57.7255368,"longitude":11.8530142},
                                            {"latitude":57.7252686,"longitude":11.8530035},
                                            {"latitude":57.7252686,"longitude":11.8530035},
                                            {"latitude":57.7250755,"longitude":11.8526709},
                                            {"latitude":57.7246785,"longitude":11.8518448},
                                            {"latitude":57.7242386,"longitude":11.8508148},
                                            {"latitude":57.7241635,"longitude":11.8505144}]; }
                                            */
    }


    /////////////////////////////////////////////////////
    /////////////////// Init ////////////////////////////
    /////////////////////////////////////////////////////

    Component.onCompleted: {
        var platform = SpaPlatform.platform;
        platform.init(Scorfa.scorfa, Lie.Promise);

        var featuresReadyPromise = new Lie.Promise(function(resolve, reject) {
             if (SpaCloud.areFeaturesLoaded()) {
                 Log.debug("QtLocationMap", "Init: Features are ready");
                 resolve();
             } else {
                 var featuresReady = function() {
                     Log.debug("QtLocationMap", "Init: Features are ready");
                     SpaCloud.onFeaturesLoaded.disconnect(featuresReady);
                     resolve();
                 }
                 SpaCloud.onFeaturesLoaded.connect(featuresReady);
             }
        });

        //: Light 24 450 1
        //: App main text
        //% "QtLocation Map Test"
        Viewer.mainText = qsTrId("100_APP_MAIN_TEXT");

        Lie.Promise.all([featuresReadyPromise])
            .then(function (results) {
                Log.debug("QtLocationMap", "Init: All asynchronous initialization is done");

                var qmlContextAppCtrl = {
                    appContainer: appContainer,
                    viewer: Viewer,
                    framework: Framework
                };

                var libraryRoots = {
                    libraryStack1RootView: libraryStack1RootView
                };

                // Dependency inject needed resources
                appController = AppController.init(qmlContextAppCtrl, libraryRoots);
                maximizedViewController = MaximizedViewController.init(appController);
                libraryStack1RootViewController = LibraryStack1RootViewController.init(appController);
                libraryStack1ViewController = LibraryStack1ViewController.init(appController);

                // update position
                mapState.currentPosition = QtNavigation.currentPosition();
                mapState.currentHeading = QtNavigation.currentHeading();

                // connect to navigation interface
                QtNavigation.onPositionUpdated.connect(appController.onPositionUpdated);
                QtNavigation.onHeadingUpdated.connect(appController.onHeadingUpdated);
                QtNavigation.onRouteEmpty.connect(appController.onRouteEmpty);
                QtNavigation.onRouteAvailable.connect(appController.onRouteAvailable);
                QtNavigation.onDestinationSet.connect(appController.onDestinationSet);
                QtNavigation.onNextManeuver.connect(appController.onNextManeuver);
                QtNavigation.onRemainingDistance.connect(appController.onRemainingDistance);

                // debug
                QtNavigation.onCurrentPath.connect(appController.onCurrentPath);
                QtNavigation.onNextInstruction.connect(appController.onNextInstruction);
                QtNavigation.onMarkerLine.connect(appController.onMarkerLine);

                maximizedViewState.state = "initialized";
                Log.debug("QtLocationMap", "initialised using " + QtNavigation.provider());
        });
    }

    Component.onDestruction: {
        QtNavigation.onPositionUpdated.disconnect(appController.onPositionUpdated);
        QtNavigation.onHeadingUpdated.disconnect(appController.onHeadingUpdated);
        QtNavigation.onRouteAvailable.disconnect(appController.onRouteAvailable);
        QtNavigation.onRouteEmpty.disconnect(appController.onRouteEmpty);
        QtNavigation.onDestinationSet.disconnect(appController.onDestinationSet);
        QtNavigation.onNextManeuver.disconnect(appController.onNextManeuver);
        QtNavigation.onRemainingDistance.disconnect(appController.onRemainingDistance);

        // debug
        QtNavigation.onCurrentPath.disconnect(appController.onCurrentPath);
        QtNavigation.onNextInstruction.disconnect(appController.onNextInstruction);
        QtNavigation.onMarkerLine.disconnect(appController.onMarkerLine);
    }
}
