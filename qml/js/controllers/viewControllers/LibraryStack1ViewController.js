function init(appController) {
    (function init() {
        Log.debug("QtLocationMap", "LibraryStack1ViewController.init()");
    })();

    function searchButtonClicked(searchTerm) {
        appController.findRouteTo(searchTerm);
    }

    return {
        init: init,
        searchButtonClicked: searchButtonClicked
    };
}
