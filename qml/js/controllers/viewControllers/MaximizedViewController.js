function init(appController) {
    (function init() {
        Log.debug("QtLocationMap", "MaximizedViewController.init()");
    })();

    function goToLibraryButtonClicked() {
        appController.libraryView();
    }

    return {
        init: init,
        goToLibraryButtonClicked: goToLibraryButtonClicked
    };
}
