function init(appController) {
    (function init() {
        Log.debug("QtLocationMap", "LibraryStack1RootViewController.init()");
    })();

    function backStack1ViewClicked() {
        appController.libraryBack();
    }

    function closeStack1ViewClicked() {
        appController.libraryStack1Close();
    }

    return {
        init: init,
        backStack1ViewClicked: backStack1ViewClicked,
        closeStack1ViewClicked: closeStack1ViewClicked
    };
}
