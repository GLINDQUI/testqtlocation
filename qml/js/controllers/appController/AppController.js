function init(qmlContext, libraryRoots) {
    (function init() {
        Log.debug("QtLocationMap", "AppController.init()");
    })();

    var libraryStack1VerticalInitialized = false;

    // Smelly and ugly. To be fixed by framework (Jonas?).
    // For more info, look in Jira: SPA-2818
    function pathPrefix() {
        if(qmlContext.viewer.runningInSimulator) {
            return "file:///";
        }
        else {
            return "file://";
        }
    }

    /* CALLS FROM LIBRARY VIEW CONTROLLERS */
    function libraryView() {
        if (!libraryStack1VerticalInitialized) {

            libraryRoots.libraryStack1RootView.push(pathPrefix() + qmlContext.framework.qmlPath + "/views/LibraryStack1View.qml");
            libraryStack1VerticalInitialized = true;
        }

        qmlContext.appContainer.viewState = "LIBRARY_STACK_1_VIEW";
        qmlContext.viewer.openLibraryView();
    }

    function libraryStack1Close() {
    }

    function calculateRouteTo(destination) {
        //Log.debug("QtLocationMap", "appController.calculateRouteTo()");

        mapState.currentRoute = [];
        QtNavigation.calculateRouteTo(destination);
    }

    function findRouteTo(destination) {
        //Log.debug("QtLocationMap", "appController.findRouteTo()");

        mapState.currentRoute = [];
        QtNavigation.findRouteTo(destination);
    }

    function onPositionUpdated(position) {
        mapState.currentPosition = position;
    }

    function onHeadingUpdated(heading) {
        mapState.currentHeading = heading;
    }

    function onRouteAvailable(path, start, end) {
        //Log.debug("QtLocationMap", "appController.onRouteAvailable()");

        mapState.currentRoute = path;
        mapState.currentRouteStartpoint = start;
        mapState.currentRouteEndpoint = end;
    }

    function onRouteEmpty() {
        //Log.debug("QtLocationMap", "appController.onRouteEmpty()");

        mapState.currentRoute = [];
        mapState.currentDestinationName = "";

        // debug
        debugState.currentPath = [];
        debugState.markerLine = [];
    }

    function onDestinationSet(name, location) {
        mapState.currentDestinationName = name;
        mapState.currentDestinationLocation = location;
    }

    function onNextManeuver(maneuver) {
        Log.debug("QtLocationMap", "appController.onNextManeuver(): next maneuver = " + JSON.stringify(maneuver));
    }

    function onRemainingDistance(distance) {
        Log.debug("QtLocationMap", "appController.onRemainingDistance(): remaining distance = " + JSON.stringify(distance));
    }

    // debug
    function onCurrentPath(path) {
        //Log.debug("QtLocationmap", "appController.onCurrentPath(): segment length = " + path.length);

        debugState.currentPath = path;
    }

    function onNextInstruction(position) {
        debugState.nextPosition = position;
    }

    function onMarkerLine(start, end) {
        debugState.markerLine = [ start, end ];
    }

    return {
        // Public functions
        init: init,
        libraryView: libraryView,
        libraryStack1Close: libraryStack1Close,
        calculateRouteTo: calculateRouteTo,
        findRouteTo: findRouteTo,
        onPositionUpdated: onPositionUpdated,
        onHeadingUpdated: onHeadingUpdated,
        onRouteAvailable: onRouteAvailable,
        onRouteEmpty: onRouteEmpty,
        onDestinationSet: onDestinationSet,
        onNextManeuver: onNextManeuver,
        onRemainingDistance: onRemainingDistance,
        onCurrentPath: onCurrentPath,
        onNextInstruction: onNextInstruction,
        onMarkerLine: onMarkerLine
    };
};
