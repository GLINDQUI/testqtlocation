import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtPositioning 5.3
import QtLocation 5.3
import SpaComponents 1.0

SpaLibraryPageContainer {
    id: libraryStack1View

    title: mapState.currentDestinationName ?
        //: Light 24 200 1
        //: Label for destination
        //% "Driving to"
        qsTrId("100_DRIVING_TO") + " " + mapState.currentDestinationName :
        //: Light 24 250 1
        //: Label for destination
        //% "Driving around"
        qsTrId("100_DRIVING_AROUND")

    Column {
        anchors.right: parent.right
        anchors.top: parent.top

        Row {
            TextField {
                id: searchTextInput

                style: TextFieldStyle {
                    textColor: "black"
                    background: Rectangle {
                        radius: 2
                        implicitWidth: searchButton.width
                        implicitHeight: searchButton.height
                        color: "lightgray"
                        border.color: "gray"
                        border.width: 1
                    }
                }

                //: Light 14 100 1
                //: Placeholder text for search term input
                //% "Enter search term"
                placeholderText: qsTrId("100_ENTER_SEARCH_TERM")

                onAccepted: {
                    libraryStack1ViewController.searchButtonClicked(searchTextInput.text);
                }
            }

            Button {
                id: searchButton

                width: 100

                //% "I'm feeling lucky"
                text: qsTrId("100_FEELING_LUCKY")

                onClicked: {
                    libraryStack1ViewController.searchButtonClicked(searchTextInput.text);
                }
            }
        }

        ComboBox {
            id: mapTypesCombo

            width: 200

            model: mapTypesList
            visible: model.count > 0

            onCurrentIndexChanged: {
                //map.activeMapType = map.supportedMapTypes[currentIndex];
            }
        }
    }

    tabs: VisualItemModel {
        SpaLibraryPage {
            Map {
                id: map

                anchors.fill: parent
                anchors.margins: - parent.width * 0.25 // increase the size of the map outside of the screen borders to allow for rotating it

                plugin: Plugin {
                    name: mapState.provider

                    PluginParameter {
                        name: "google.maps.apikey"
                        value: mapState.parameters["google.maps.apikey"]
                    }

                    PluginParameter {
                        name: "google.maps.baseuri"
                        value: mapState.parameters["google.maps.baseuri"]
                    }

                    PluginParameter {
                        name: "google.maps.basepath"
                        value: mapState.parameters["google.maps.basepath"]
                    }

                    PluginParameter {
                        name: "google.places.apikey"
                        value: mapState.parameters["google.places.apikey"]
                    }

                    PluginParameter {
                        name: "google.places.baseuri"
                        value: mapState.parameters["google.places.baseuri"]
                    }

                    PluginParameter {
                        name: "google.places.basepath"
                        value: mapState.parameters["google.places.basepath"]
                    }

                    PluginParameter {
                        name: "google.directions.apikey"
                        value: mapState.parameters["google.directions.apikey"]
                    }

                    PluginParameter {
                        name: "google.directions.baseuri"
                        value: mapState.parameters["google.directions.baseuri"]
                    }

                    PluginParameter {
                        name: "google.directions.basepath"
                        value: mapState.parameters["google.directions.basepath"]
                    }

                    /*
                    PluginParameter {
                        name: "tomtom.maps.apikey"
                        value: mapState.parameters["tomtom.maps.apikey"]
                    }

                    PluginParameter {
                        name: "tomtom.maps.baseuri"
                        value: mapState.parameters["tomtom.maps.baseuri"]
                    }
                    */

                    /*
                    PluginParameter {
                        name: "here.app_id"
                        value: mapState.parameters["here.app_id"]
                    }

                    PluginParameter {
                        name: "here.token"
                        value: mapState.parameters["here.token"]
                    }

                    PluginParameter {
                        name: "here.proxy"
                        value: mapState.parameters["here.proxy"]
                    }

                    PluginParameter {
                        name: "here.mapping.host"
                        value: mapState.parameters["here.mapping.host"]
                    }
                    */
                }

                center: mapState.currentPosition
                rotation: 360 - mapState.currentHeading
                zoomLevel: maximumZoomLevel - 4

                MapPolyline {
                    path: mapState.currentRoute
                    line.width: 12
                    line.color: "blue"
                    opacity: 0.2
                    smooth: true
                }

                MapPolyline {
                    path: mapState.currentRoute
                    line.width: 9
                    line.color: "lightblue"
                    opacity: 0.7
                    smooth: true
                }

                MapCircle {
                    radius: 5
                    color: "lightblue"
                    border.color: "blue"
                    opacity: 0.7

                    Binding on center {
                        when: !!mapState.currentRouteStartpoint
                        value: mapState.currentRouteStartpoint
                    }
                }

                MapCircle {
                    radius: 5
                    color: "darkgreen"
                    border.color: "transparent"
                    opacity: 1

                    Binding on center {
                        when: !!mapState.currentDestinationLocation
                        value: mapState.currentDestinationLocation
                    }
                }

                MapCircle {
                    radius: 20
                    color: "lightgreen"
                    border.color: "green"
                    opacity: 0.5

                    Binding on center {
                        when: !!mapState.currentDestinationLocation
                        value: mapState.currentDestinationLocation
                    }
                }

                /*
                 * This needs some trigonometry magic to make the
                 * anchor point not relocate during rotation.
                MapQuickItem {
                    id: destinationMarker

                    anchorPoint.x: image.width / 2
                    anchorPoint.y: image.height / 2

                    rotation: -map.rotation
                    visible: mapState.currentDestinationName !== ""
                    Binding on coordinate {
                        when: !!mapState.currentDestinationLocation
                        value: mapState.currentDestinationLocation
                    }

                    sourceItem: Image {
                        id: image

                        width: sourceSize.width
                        height: sourceSize.height

                        source: Theme.path + "img_nav_destination.png"
                    }
                }
                */

                // debug
                MapCircle {
                    radius: 5
                    color: "darkblue"
                    border.color: "transparent"
                    opacity: 1
                    center: map.center
                }

                // debug
                MapCircle {
                    radius: 10
                    color: "lightblue"
                    border.color: "blue"
                    opacity: 0.6
                    center: map.center
                }

                // debug
                MapCircle {
                    radius: 5
                    color: "darkred"
                    border.color: "transparent"
                    opacity: 1

                    Binding on center {
                        when: !!debugState.nextPosition
                        value: debugState.nextPosition
                    }
                }

                // debug
                MapCircle {
                    radius: 10
                    color: "salmon"
                    border.color: "red"
                    opacity: 0.6

                    Binding on center {
                        when: !!debugState.nextPosition
                        value: debugState.nextPosition
                    }
                }

                // debug
                MapPolyline {
                    path: debugState.markerLine
                    line.color: "red"
                    line.width: 2
                }

                // debug
                MapPolyline {
                    path: debugState.currentPath
                    line.color: "green"
                    line.width: 2

                    onPathChanged: {
                        Log.debug("QtLocationMap", "LibraryStack1View.Plugin: segment path changed to " + path.length + " elements");
                    }
                }

                Component.onCompleted: {
                    var mapTypes = supportedMapTypes;
                    mapTypesList.clear();
                    for(var i = 0, l = mapTypes.length; i < l; i++) {
                        mapTypesList.append({ modelData: mapTypes[i].name, index: i, raised: false });
                    }
                    mapTypesCombo.currentIndex = Math.min(8, i - 1);
                }
            }

            /*
            Image {
                anchors.centerIn: parent

                source: Theme.path + "img_nav_car_2d_00.png"
            }
            */

            Column {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 20

                spacing: 10

                Image {
                    source: Theme.path + "img_nav_zoom_plus.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            map.zoomLevel = Math.min(map.zoomLevel + 1, map.maximumZoomLevel);
                        }
                    }
                }

                Image {
                    source: Theme.path + "img_nav_zoom_minus.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            map.zoomLevel = Math.max(map.zoomLevel - 1, map.minimumZoomLevel);
                        }
                    }
                }

                Image {
                    source: Theme.path + "img_nav_car_2d_00.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            QtNavigation.nextSegment();
                        }
                    }
                }
            }
        }
    }
}
