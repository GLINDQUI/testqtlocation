import QtQuick 2.0
import SpaComponents 1.0

SpaMaximizedView {
    id: maximizedView

    visible: Viewer.renderState === SpaApplicationViewer.RenderStateExpanded
    state: maximizedViewState.state

    Item {
        x: 20
        y: 160

        SpaHomescreenToolbar {
            id: maximizedViewToolbar

            anchors.top: parent.top
            anchors.left: parent.left

            textButtonCount: 1

            //: Light 24 200 1
            //: open map button label
            //% "Show map"
            buttonText1: qsTrId("100_SHOW_MAP")

            onButton1Clicked: {
                maximizedViewController.goToLibraryButtonClicked();
            }
        }

        SpaLabel {
            id: welcomeText

            anchors.top: maximizedViewToolbar.top
            anchors.bottom: maximizedViewToolbar.bottom
            anchors.left: maximizedViewToolbar.right
            anchors.leftMargin: 20
            width: 400

            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            wrapMode : Text.WordWrap
            fontClass: "InfoText"

            //: Light 24 500 10
            //: informational and/or welcome text
            //% "QtLocation Map Test"
            text: qsTrId("100_INFO")
        }

        SpaActivityIndicatorLarge {
            id: maximizedSpinner
            visible: false
            x: 400
            y: 100
            z: 100
            showPercent: false
            infinite: true
            abortable: false
        }
    }

    states: [
        State {
            name: "initialized"

            PropertyChanges {
                target: maximizedSpinner
                visible: false
            }

            PropertyChanges {
                target: welcomeText
                visible: true
            }

            PropertyChanges {
                target: maximizedViewToolbar
                enabled: true
            }
        },
        State {
            name: "initializing"

            PropertyChanges {
                target: maximizedSpinner
                visible: true
            }

            PropertyChanges {
                target: welcomeText
                visible: false
            }

            PropertyChanges {
                target: maximizedViewToolbar
                enabled: false
            }
        }
    ]
}
