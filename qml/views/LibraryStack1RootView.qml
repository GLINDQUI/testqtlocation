import QtQuick 2.0
import SpaComponents 1.0

SpaLibraryWindow {
    visible: Viewer.renderState === SpaApplicationViewer.RenderStatePageGroupView && appContainer.viewState === "LIBRARY_STACK_1_VIEW";

    onCloseClicked: {
        libraryStack1RootViewController.closeStack1ViewClicked();
    }
}
