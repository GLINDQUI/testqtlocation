#include "navigation.h"

#include "Log.h"
#include <QDebug>
#include <typeinfo>

#include <QGeoCircle>
#include <QGeoManeuver>
#include <QGeoRoute>
#include <QGeoRoutingManager>
#include <QPlaceManager>
#include <QPlaceResult>
#include <QPlaceSearchRequest>

Navigation::Navigation(QObject *parent) :
    QObject(parent),
    _naviInterface(),
    _parameters(),
    _serviceProvider(0),
    _placeManager(0),
    _routingManager(0),
    _searchReply(0) {
    _naviInterface.setApiEnabled();
    connect(&_naviInterface, SIGNAL(qLocationUpdated(QVariant)), this, SLOT(onPositionUpdated(QVariant)));

    initServiceProvider();
    initManagers();
}

Navigation::~Navigation()
{
    _naviInterface.disconnect();

    if (_searchReply) {
        _searchReply->deleteLater();
    }

    _routingManager->disconnect();
    if (_routingManager) {
        _routingManager->deleteLater();
    }

    _placeManager->disconnect();
    if (_placeManager) {
        _placeManager->deleteLater();
    }

    if (_serviceProvider) {
        _serviceProvider->deleteLater();
    }

    clearRoute();
}

QString Navigation::provider()
{
    return _provider;
}

QVariantMap Navigation::pluginParameters()
{
    return _parameters;
}

QGeoCoordinate Navigation::currentPosition()
{
    QGeoCoordinate geoCoordinate;
    geoCoordinate.setLatitude(_currentPosition["latitude"].toDouble());
    geoCoordinate.setLongitude(_currentPosition["longitude"].toDouble());
    return geoCoordinate;
}

QGeoCoordinate Navigation::previousPosition()
{
    QGeoCoordinate geoCoordinate;
    geoCoordinate.setLatitude(_previousPosition["latitude"].toDouble());
    geoCoordinate.setLongitude(_previousPosition["longitude"].toDouble());
    return geoCoordinate;
}

QVariantMap Navigation::geoCoordinateToVariantMap(QGeoCoordinate coordinate)
{
    QVariantMap result;
    result.insert("latitude", coordinate.latitude());
    result.insert("longitude", coordinate.longitude());
    return result;
}

double Navigation::currentHeading()
{
    return _currentPosition["heading"].toDouble();
}

void Navigation::calculateRouteTo(QVariant destination) {
    calculateRouteTo(QGeoCoordinate(destination.toMap()["latitude"].toDouble(), destination.toMap()["longitude"].toDouble()));
}

void Navigation::initServiceProvider()
{
    _provider = "google";

    _parameters.insert("google.maps.apikey", "bork");
    _parameters.insert("google.maps.baseuri", "maps.googleapis.com");
    _parameters.insert("google.maps.basepath", "maps/vt");

    _parameters.insert("google.places.apikey", "AIzaSyDkN7erPZqxLP5RiOcflvq-iWj7plF57Rc");
    _parameters.insert("google.places.baseuri", "maps.googleapis.com");
    _parameters.insert("google.places.basepath", "maps/api/place/textsearch");

    _parameters.insert("google.directions.apikey", "AIzaSyDV9peXfom4kkpfw2aiYOzNeIVmv4XDKJg");
    _parameters.insert("google.directions.baseuri", "maps.googleapis.com");
    _parameters.insert("google.directions.basepath", "maps/api/directions");

    _parameters.insert("tomtom.maps.apikey", "33cxb7pkegg88ypmnnry63ke");
    _parameters.insert("tomtom.maps.baseuri", "api.tomtom.com");
    _parameters.insert("tomtom.route.apikey", "f2dwc598dmyzqeqwhhhnhh7k");

    _serviceProvider = new QGeoServiceProvider(_provider, _parameters);
}

void Navigation::initManagers()
{
    if (_serviceProvider) {
        _placeManager = _serviceProvider->placeManager();
        connect(_placeManager, SIGNAL(finished(QPlaceReply*)), this, SLOT(onSearchFinished(QPlaceReply*)));
        connect(_placeManager, SIGNAL(error(QPlaceReply*,QPlaceReply::Error,QString)), this, SLOT(onSearchError(QPlaceReply*,QPlaceReply::Error,QString)));

        _routingManager = _serviceProvider->routingManager();
        connect(_routingManager, SIGNAL(finished(QGeoRouteReply*)), this, SLOT(onRouteFinished(QGeoRouteReply*)));
        connect(_routingManager, SIGNAL(error(QGeoRouteReply*,QGeoRouteReply::Error,QString)), this, SLOT(onRouteError(QGeoRouteReply*,QGeoRouteReply::Error,QString)));
    }
}

double Navigation::calculateDistance(QGeoCoordinate position, QGeoCoordinate start, QGeoCoordinate end) {
    double result = std::numeric_limits<float>::max();

    try {
        double pLat = position.latitude();
        double pLong = position.longitude();
        double sLat = start.latitude();
        double sLong = start.longitude();
        double eLat = end.latitude();
        double eLong = end.longitude();

        double dLat = eLat - sLat;
        double dLong = eLong - sLong;

        if (dLat == 0 && dLong == 0) {
            // start and end of the line are the same point
            result = position.distanceTo(start);
        } else {
            double coefficient = ((pLat - sLat) * (dLat) + (pLong - sLong) * (dLong)) / ((dLat * dLat) + (dLong * dLong));
            if (coefficient <= 0) {
                result = position.distanceTo(start);
            } else if (coefficient >= 1){
                result = position.distanceTo(end);
            } else {
                QGeoCoordinate pointOnLine = QGeoCoordinate(start.latitude() + coefficient * dLat, start.longitude() + coefficient * dLong);
                result = position.distanceTo(pointOnLine);
            }
        }
    } catch (QException ex) {
        Log::error("Navigation", QString("calculateDistance(): error = %1").arg(ex.what()));
    }

    return result;
}

double Navigation::calculateRemainingDistance()
{
    double result = std::numeric_limits<float>::max();

    long index = -1;
    double minDistance = std::numeric_limits<float>::max();
    for(QGeoCoordinate coordinate: _currentSegment.path()) {
        double distance = currentPosition().distanceTo(coordinate);
        if (distance < minDistance) {
            minDistance = distance;
            index = _currentSegment.path().indexOf(coordinate);
        }
    }

    if (index != -1) {
        result = 0;
        for (long i = index; i < _currentSegment.path().length(); i++) {
            result += currentPosition().distanceTo(_currentSegment.path().at(i));
        }
        result += currentPosition().distanceTo(_nextSegment.maneuver().position());
    }


    return result;
}

void Navigation::calculateRouteTo(QGeoCoordinate destination) {
    clearRoute();
    if (_routingManager) {
        _routingManager->calculateRoute(QGeoRouteRequest(currentPosition(), destination));
    }
}

void Navigation::clearRoute()
{
    _nextSegment = QGeoRouteSegment();
}

void Navigation::emitNextManeuver()
{
    QGeoManeuver qGeoManeuver = _nextSegment.maneuver();
    QVariantMap maneuver;

    switch(qGeoManeuver.direction()) {
        case QGeoManeuver::DirectionForward:
            maneuver.insert("direction", "forward");
            break;
        case QGeoManeuver::DirectionBearRight:
            maneuver.insert("direction", "bearright");
            break;
        case QGeoManeuver::DirectionLightRight:
            maneuver.insert("direction", "lightright");
            break;
        case QGeoManeuver::DirectionRight:
            maneuver.insert("direction", "right");
            break;
        case QGeoManeuver::DirectionHardRight:
            maneuver.insert("direction", "hardright");
            break;
        case QGeoManeuver::DirectionUTurnRight:
            maneuver.insert("direction", "uturnright");
            break;
        case QGeoManeuver::DirectionUTurnLeft:
            maneuver.insert("direction", "uturnleft");
            break;
        case QGeoManeuver::DirectionHardLeft:
            maneuver.insert("direction", "hardleft");
            break;
        case QGeoManeuver::DirectionLeft:
            maneuver.insert("direction", "left");
            break;
        case QGeoManeuver::DirectionLightLeft:
            maneuver.insert("direction", "lightleft");
            break;
        case QGeoManeuver::DirectionBearLeft:
            maneuver.insert("direction", "bearleft");
            break;
        default:
            maneuver.insert("direction", "");
    }

    maneuver.insert("instruction", qGeoManeuver.instructionText());

    emit nextManeuver(maneuver);
    emit nextInstruction(_nextSegment.maneuver().position());

    QVariantList path;
    for (QGeoCoordinate coordinate: _currentSegment.path()) {
        path.append(geoCoordinateToVariantMap(coordinate));
    }
    emit currentPath(path);

    _currentSegment = QGeoRouteSegment(_nextSegment);
    _nextSegment = QGeoRouteSegment(_nextSegment.nextRouteSegment());
}

void Navigation::emitRemainingDistance()
{
    emit remainingDistance(calculateRemainingDistance());
}

void Navigation::findRouteTo(QString searchTerm)
{
    QPlaceSearchRequest searchRequest;
    searchRequest.setSearchTerm(searchTerm);
    searchRequest.setSearchArea(QGeoCircle(currentPosition(), 20000));
    _searchReply = _placeManager->search(searchRequest);
}

void Navigation::nextSegment()
{
    emitNextManeuver();
}

void Navigation::onPositionUpdated(QVariant position)
{
    QVariantMap positionMap = position.toMap();

    if (positionMap["latitude"] == 0 && positionMap["longitude"] == 0) {
        return;
    }

    if (positionMap["heading"] != _currentPosition["heading"]) {
        _currentPosition["heading"] = positionMap["heading"];
        emit headingUpdated(_currentPosition["heading"].toDouble());
    }

    if (positionMap["latitude"] != _currentPosition["latitude"] || positionMap["longitude"] != _currentPosition["longitude"]) {
        _previousPosition = _currentPosition;
        _currentPosition["latitude"] = positionMap["latitude"];
        _currentPosition["longitude"] = positionMap["longitude"];
        emit positionUpdated(currentPosition());
        if (previousPosition().latitude() != 0 || previousPosition().longitude() != 0) {
            emit markerLine(previousPosition(), currentPosition());
        }

        if (_nextSegment.isValid()) {
            qreal distance = currentPosition().distanceTo(_nextSegment.maneuver().position());
            qDebug() << "distance" << distance;
            if (distance <= 10.0) {
                emitNextManeuver();
            } else {
                distance = calculateDistance(_nextSegment.maneuver().position(), previousPosition(), currentPosition());
                qDebug() << "line distance" << distance;
                if (distance <= 25.0) {
                    emitNextManeuver();
                }
            }

            emitRemainingDistance();
        }
    }
}

void Navigation::onRouteError(QGeoRouteReply* reply, QGeoRouteReply::Error error, QString errorString)
{
    Q_UNUSED(reply)
    Q_UNUSED(error)

    Log::error("Navigation", QString("onRouteError(): error = %1").arg(errorString));
}

void Navigation::onRouteFinished(QGeoRouteReply* reply) {
    if (reply->error() == QGeoRouteReply::NoError) {
        _path = reply->routes().first().path();
        QVariantList route;
        foreach (QGeoCoordinate coordinate, _path) {
            route.append(geoCoordinateToVariantMap(coordinate));
        }
        emit routeAvailable(route, _path.first(), _path.last());
        _currentSegment = QGeoRouteSegment(reply->routes().first().firstRouteSegment());
        _nextSegment = QGeoRouteSegment(_currentSegment);
        emitNextManeuver();
    } else {
        emit routeEmpty();
    }
}

void Navigation::onSearchError(QPlaceReply *reply, QPlaceReply::Error error, QString errorString)
{
    Q_UNUSED(reply)
    Q_UNUSED(error)

    Log::error("Navigation", QString("onSearchError(): error = %1").arg(errorString));
}

void Navigation::onSearchFinished(QPlaceReply *reply)
{
    if (reply->error() == QPlaceReply::NoError) {
        foreach (QPlaceSearchResult result, _searchReply->results()) {
            if (result.type() == QPlaceSearchResult::PlaceResult) {
                QPlaceResult place = result;
                emit destinationSet(place.place().name(), place.place().location().coordinate());
                calculateRouteTo(place.place().location().coordinate());
                return;
            }
        }
    }
}
